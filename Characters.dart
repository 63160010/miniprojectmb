import 'dart:io';
mixin HP{
  void hps(){
  }
}
mixin Damage{
  void damages() {
  }
}
class Characters with HP,Damage{
  int hp;
  int damage;
  String symbol;
  Characters(this.hp,this.damage,this.symbol);
  
  int getHp()=>hp;
  int getDamage()=>damage;
  String getSymbol()=> symbol;
  void setHp(int hp)=>this.hp=hp;
  void setDamage(int damage)=>this.damage=damage;
  void setSymbol(String symbol)=>this.symbol=symbol;

}