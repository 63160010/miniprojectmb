import 'dart:io';
import 'Characters.dart';
import 'Maps.dart';
import 'Player.dart';
import 'Weapon.dart';

class Game {
  Characters Empire = Characters(100, 15, '\u{1F978}');
  Characters Nerva = Characters(80, 20, '\u{1F47F}');
  Characters Marxism = Characters(120, 10, '\u{1F479}');
  Characters Trinity = Characters(70, 25, '\u{1F47E}');

  Player p1 = Player(70, 25, '\u{1F47E}', 0);
  Player p2 = Player(100, 15, '\u{1F978}', 4);

  Weapon Blade = Weapon(0, 10);
  Weapon Katana = Weapon(5, 5);
  Weapon Keyblade = Weapon(3, 7);
  Weapon Rebellion = Weapon(10, 0);

  Maps maps = Maps();
  showMap() {
    maps.setMap(p1, p2);
    stdout.write("Player1 HP :");stdout.write(p1.getHp());stdout.write('        ');stdout.write("Damage :");stdout.write(p1.getDamage());
    stdout.write('      ');
    stdout.write("Player2 HP :"); stdout.write(p2.getHp());stdout.write('        ');stdout.write("Damage :");print(p2.getDamage());
    maps.showMaps();
  }

  ChooseChar(int n, int ability) {
    Player p;
    if (n == 1) {
      p = p1;
    } else {
      p = p2;
    }
    if (ability == 1) {
      p.setHp(Empire.hp);
      p.setDamage(Empire.damage);
      p.setSymbol(Empire.symbol);
    } else if (ability == 2) {
      p.setHp(Nerva.hp);
      p.setDamage(Nerva.damage);
      p.setSymbol(Nerva.symbol);
    } else if (ability == 3) {
      p.setHp(Marxism.hp);
      p.setDamage(Marxism.damage);
      p.setSymbol(Marxism.symbol);
    } else if (ability == 4) {
      p.setHp(Trinity.hp);
      p.setDamage(Trinity.damage);
      p.setSymbol(Trinity.symbol);
    }
    print(p.symbol);
  }
  inputPlayer(int n) {
    int? ability = int.parse(stdin.readLineSync()!);
    if(ability>0 && ability<=4){
      ChooseChar(n, ability);
    }
    else{
      print("error Enter a number that exceeds the table!!, You get the default character");
      ChooseChar(n, ability);
    }
  }

  UseSkill(int n, int skill) {
    Player p;
    if (n == 1) {
      p = p1;
    } else {
      p = p2;
    }

    switch (skill) {
      case 1:
        {
          if (n == 1 && p1.getX() + 1 == p2.getX()) {
            int attack = p2.getHp() - p1.getDamage();
            p2.setHp(attack);
          } else if (n == 2 && p2.getX() - 1 == p1.getX()) {
            int attack = p1.getHp() - p2.getDamage();
            p1.setHp(attack);
          }
        }
        break;
      case 2:
        {
          if (n == 1) {
            int heal = p1.getHp() + 5;
            p1.setHp(heal);
          } else {
            int heal = p2.getHp() + 5;
            p2.setHp(heal);
          }
        }
        break;
      case 3:
        {
          if (n == 1 && p1.getX() + 1 < p2.getX()) {
            maps.settable(p1.getX());
            int walkOn = p1.getX() + 1;
            p1.setX(walkOn);
          } else if (n == 2 && p2.getX() - 1 > p1.getX()) {
            maps.settable(p2.getX());
            int walkOn = p2.getX() - 1;
            p2.setX(walkOn);
          }
        }
        break;
      case 4:
        {
          if (n == 1 && p1.getX() > 0) {
            maps.settable(p1.getX());
            int reverse = p1.getX() - 1;
            p1.setX(reverse);
          } else if (n == 2 && p2.getX() < 4) {
            maps.settable(p2.getX());
            int reverse = p2.getX() + 1;
            p2.setX(reverse);
          }
        }
        break;
    }
  }
  inputSkPlayer(int n) {
    int? skill = int.parse(stdin.readLineSync()!);
    if(skill>0 && skill<=4){
      UseSkill(n, skill);
    }
    else{
      print("error Enter a number that exceeds the table!!, You didn't do anything");
      UseSkill(n, skill);
    }
  }

  Weapons(int e, int weapon) {
    Player a;
    if (e == 1) {
      a = p1;
    } else {
      a = p2;
    }
    int hp = a.getHp();
    int damage = a.getDamage();
    if (weapon == 1) {
      a.setDamage(Blade.getDamage() + damage);
    } else if (weapon == 2) {
      a.setHp(Katana.getHp() + hp);
      a.setDamage(Katana.getDamage() + damage);
    } else if (weapon == 3) {
      a.setHp(Keyblade.getHp() + hp);
      a.setDamage(Keyblade.getDamage() + damage);
    } else if (weapon == 4) {
      a.setHp(Rebellion.getHp() + hp);
    }
  }
  inputWeaPlayer(int e) {
    int? weapon = int.parse(stdin.readLineSync()!);
    if(weapon>0 && weapon<=4){
      Weapons(e, weapon);
    }
    else{
      print("error Enter a number that exceeds the table!! , you didn't get weapon");
    }
  }
  ///////////
  showIntChar() {
    print("Please select a character?");
    print("1 Empire HP: 100 Damage:15 ");
    print("2 Nerva HP: 80 Damage:20 ");
    print("3 Marxism HP: 120 Damage:10 ");
    print("4 Trinity HP: 70 Damage:25 ");
  }
  showSkill() {
    print("Please select a skill?");
    print("1 attack");
    print("2 heal");
    print("3 walOn");
    print("4 reverse");
  }
  showWeap() {
    print("Please select a weapons?");
    print("1 Blade Damage:10");
    print("2 Katana HP:5 Damage:5");
    print("3 Keyblade HP:3 Damage:7");
    print("4 Rebellion HP:10");
  }

  bool checkEnd(){
  
	if(p1.getHp()<=0){
		stdout.write("Player 2    ");stdout.write(p2.getSymbol()); print('   win!');
		return true;
	} else if (p2.getHp()<=0){
		stdout.write("Player 1    ");stdout.write(p1.getSymbol()); print('   win!');
		return true;
	}
	return false;
}
}

void main() {
  Game game = Game();
  print("Wellcom to Game");

  game.showIntChar();
  print("Player 1");
  game.inputPlayer(1);

  game.showIntChar();
  print("Player 2");
  game.inputPlayer(2);

  var c = 1;
  while (true) {
    if (game.checkEnd()) {
      game.showMap();
      break;
    }
    if (c == 2) {
      game.showMap();

      game.showSkill();
      print("Player 1");
      game.showMap();

      game.inputSkPlayer(1);
      game.showMap();
      //
      if (game.checkEnd()) {
      game.showMap();
      break;
    }
      game.showWeap();
      print("Player 2");
      game.inputWeaPlayer(2);
      game.showMap();

    } else if (c == 3) {
      game.showWeap();
      print("Player 1");
      game.inputWeaPlayer(1);
      game.showMap();
      //
      if (game.checkEnd()) {
      
      break;
    }
      game.showMap();
      game.showSkill();
      print("Player 2");
      game.inputSkPlayer(2);

    } else {
      game.showMap();
      game.showSkill();
      print("Player 1");
      game.inputSkPlayer(1);

      if (game.checkEnd()) {
      game.showMap();
      break;
    }

      game.showMap();
      game.showSkill();
      print("Player 2");
      game.inputSkPlayer(2);
    }
    c++;
  }
}